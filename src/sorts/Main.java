package kz.aitu.Week8;

import kz.aitu.Week9.InsertionSort;

public class Main {


        public static void main(String[] args) {
            int[] arr = new int[50];
            inputData(arr);

            BubbleSort bubble = new BubbleSort();

            InsertionSort insertion = new InsertionSort();
            MergeSort merge = new MergeSort();
            QuickSort quick = new QuickSort();

            System.out.println("MY DATA");
            printAll(arr);

            System.out.println("BUBBLE SORTED=");
            bubble.bubblesort(arr);
            printAll(arr);



            System.out.println("INSERTION SORTED=");
            insertion.insertionSort(arr);
            printAll(arr);

            System.out.println("MERGE SORTED=");
            merge.mergesort(arr);
            printAll(arr);

            System.out.println("QUICK SORTED");
            quick.quicksort(arr);
            printAll(arr);
        }

        private static void printAll(int[] array) {
            System.out.print("==> ");
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i]);
            }
            System.out.println();
        }

    private static void inputData(int[] array) {

        }
    }