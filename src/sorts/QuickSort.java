package kz.aitu.Week8;

public class QuickSort {
    public static void quicksort(int[] array) {
        Quicksort(array,  0, array.length - 1);
    }

    private static void Quicksort(int[] array, int leftstart, int rightend) {
        if (leftstart >= rightend) {
            return;
        }

        int middle = (leftstart + rightend) / 2;
        int pivot = array[middle];
        int i = leftstart, j = rightend;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }

            while (array[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (leftstart < j)
            Quicksort(array, leftstart, j);

        if (rightend > i)
            Quicksort(array, i, rightend);

    }



    public void printall(int [] arr){
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

}
