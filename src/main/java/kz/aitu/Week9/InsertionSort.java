package kz.aitu.Week9;

public class InsertionSort {
    public static void insertionSort(int array[]) {
        int size = array.length;
        for (int j = 1; j < size; j++) {
            int key = array[j];
            int i = j-1;
            while ( (i > -1) && ( array [i] > key ) ) {
                array [i+1] = array [i];
                i--;
            }
            array[i+1] = key;
        }
    }

    static void printArray(int arr[])
    {
        int n = arr.length;
        for (int i=0; i<n; ++i) {
            System.out.print(arr[i] + " ");
        }
    }

}
