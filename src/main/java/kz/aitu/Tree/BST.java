package kz.aitu.Tree;

public class BST {
    Node root;

    public int findMinimum() {
        int tempValue = 0;

        if (root != null) {

            root = root.getLeft();

            if (root.getLeft() != null) tempValue = root.getData();
        }

        return tempValue;
    }
}

